import argparse
import numpy as np
import os
import importlib
from scipy.io.wavfile import write
from scipy.io.wavfile import read
import sounddevice as sd
from matplotlib import pyplot as plt
import math

def tone_args(): #Basis for this was copied from the provided code in libtones for the tones assignment
    argp = argparse.ArgumentParser()
    argp.add_argument(
        "--volume",
        help="volume in 3dB units (default 9 = 0dB, 0 = 0 output)",
        type=np.float64,
        default=9,
    )
    argp.add_argument(
        "--pitch",
        help="pitch modifier in quotation marks separated by spaces in half steps. e.g. \"5, 12, 17\" ",
        type=str,
        default="5 12 17",
    )
    argp.add_argument(
        "--pmodule",
        help="pitch module to use. Try typing \"--pmodule rs\" to use our custom pitch changer",
        type=str,
        default="lr",
    )
    argp.add_argument(
        "--graph",
        action='store_true', #https://stackoverflow.com/questions/8259001/python-argparse-command-line-flags-without-arguments
    )
    argp.add_argument(
        "--out",
        help="write to WAV file instead of playing",
    )
    argp.add_argument(
        "--wav", 
        help="input audio file",
        default="test.wav",
    )
    return argp.parse_args()

def getGain(s):
    if s < 0.1:
        return 0
    db = 3 * (s - 9)
    return math.pow(10.0,(db/20.0))

args = tone_args()

module = args.pmodule + ".py"

tree = os.listdir('modules')

if not (module in tree):
    print("Module not found")
    exit()

(sampleRate, data) = read(args.wav)

data = data[:,0]

data = np.array(data, dtype=np.float32)

data /= 2^16

pitchShifter = importlib.import_module("modules." + args.pmodule, package="module") #https://stackoverflow.com/questions/42401495/how-to-dynamically-import-modules

def processChannel(channel):
    pitches = args.pitch.split(' ')
    original = np.copy(channel)
    for pitch in pitches:
        modifier = pitchShifter.pitchShift(np.copy(original), sampleRate, int(pitch))
        backPadding =  np.zeros(int(max(0,len(channel) - len(modifier))), dtype=np.float32)
        channel= channel + (np.concatenate([modifier, backPadding]))[:len(channel)]
    return channel / (len(pitches)+1)

try:
    AudioInL = data[:,0]
    AudioInR = data[:,1]
    data = np.append(processChannel(AudioInL), processChannel(AudioInR), axis=1)
except:
    data = processChannel(data)

data *= getGain(args.volume)

data = np.array(data * (2^16), dtype=np.int16) 

print("Finished processing...")

if (args.graph):
    plt.plot(data)

if args.out == None:
    sd.play(data)
else:
    write(args.out,sampleRate,data)
if (args.graph):
    plt.show()

sd.wait(ignore_errors=False)
