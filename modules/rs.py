import numpy as np
import scipy as sci
from scipy import signal 
from scipy.io.wavfile import write
from scipy.io.wavfile import read
from matplotlib import pyplot as plt # this lets you draw inline pictures in the notebooks
import pylab # this allows you to control figure size 
pylab.rcParams['figure.figsize'] = (20.0, 16.0) # this controls figure size in the notebook
import sounddevice as sd
import cv2 as cv
import math
#################
def pitchShift(data, SampleRate, pitch):
    pitch = float(2) ** (-pitch/12)
    try:
        AudioInL = np.array(data[:,0], dtype=np.float32)
        AudioInR = np.array(data[:,1],dtype=np.float32)
    except:

        AudioInL = np.array(data, dtype=np.float32)
    #Accept Inputs (Mono or Stereo)
    #ReadIn

    GrainLen = 4000
    PitchShift = pitch
    #The two variables that are at play here
    #The higher the grainlen, the better the performance

    LengthAudio = len(AudioInL)
    NumberGrains = math.floor(LengthAudio/GrainLen)
    UsableLen = AudioInL[0:NumberGrains*GrainLen]
    #CutDown Audio Such That it will compute to Grainsize

    StartPoint = np.linspace(0,LengthAudio, NumberGrains+1)[0:NumberGrains].astype(float)
    #Forget what it does but used somewhere else
    CutupSamples = np.vstack(np.split(UsableLen, NumberGrains))
    Offset = range(int(GrainLen/2),int(len(UsableLen)-GrainLen/2))
    CutupSamplesOdd = np.vstack(np.split(UsableLen[Offset], NumberGrains-1))
    #Split and stackthem into a vertical set of grains for processing
    #Reconstruct = CutupSamples.flatten()
    #Takes all the cutup elements and makes them into a vector again (only works with boxcar)
    ######################
    #Main Alghorithm
    Upscale = np.zeros([len(StartPoint),GrainLen])
    UpscaleOdd = np.zeros([len(StartPoint)-1,GrainLen])
    #Needed for for loop
    if PitchShift >= 1:
        for n in range(len(StartPoint)):
            Upscale[n,:] = np.transpose(cv.resize(CutupSamples[n,:], [1,int(GrainLen*PitchShift)],\
                                              interpolation = 2)[0:GrainLen])
        for n in range(len(StartPoint)-1):
            UpscaleOdd[n,:] = np.transpose(cv.resize(CutupSamplesOdd[n,:], [1,int(GrainLen*PitchShift)],\
                                              interpolation = 2)[0:GrainLen])
    #Scales by pitchshift as a ratio (use a just intonation ratio table), then cuts down to
    #an appropriate length
    else:
        for n in range(len(StartPoint)):
            spedup = cv.resize(CutupSamples[n,:], [1,int(GrainLen*PitchShift)],interpolation = 2)
            firstTime = 0
            while len(spedup) < GrainLen:
                if firstTime == 0: 
                    spedup = np.append(spedup[0:len(spedup)-1], list(reversed(spedup)))
                    firstTime = 1
                else:
                    spedup = np.append(spedup[0:len(spedup)-1], spedup)
            Upscale[n,:] = np.transpose(spedup[0:GrainLen])
            spedup = 0
        for n in range(len(StartPoint)-1):
            spedupOdd = cv.resize(CutupSamplesOdd[n,:], [1,int(GrainLen*PitchShift)],interpolation = 2)
            firstTime = 0
            while len(spedupOdd) < GrainLen:
                if firstTime == 0: 
                    spedupOdd = np.append(spedupOdd[0:len(spedupOdd)-1], list(reversed(spedupOdd)))
                    firstTime = 1
                else:
                    spedupOdd = np.append(spedupOdd[0:len(spedupOdd)-1], spedupOdd)
            UpscaleOdd[n,:] = np.transpose(spedupOdd[0:GrainLen])
            spedupOdd = 0
    ####Resamples to the degree specificied in Pitch Shift, then cuts off excess amounts for block.
    #If pitched up, will repitch the sample, append a reversed version, then append until it meets
    #grain length requirements
        
    #####Windows
        #window = np.blackman(GrainLen)
    window = np.bartlett(GrainLen)
    UpscaleWindowed = np.zeros([len(StartPoint),GrainLen])
    UpscaleWindowedOdd = np.zeros([len(StartPoint)-1,GrainLen])
    for n in range(len(StartPoint)):
        UpscaleWindowed[n, :] = np.multiply(Upscale[n,:], window)
    for n in range(len(StartPoint)-1):
        UpscaleWindowedOdd[n, :] = np.multiply(UpscaleOdd[n,:], window)

    Reconstruct2 = np.array(UpscaleWindowed.flatten(),dtype=np.float32)
    Reconstruct2Odd = np.array(UpscaleWindowedOdd.flatten(),dtype=np.float32)

    #Reconstructs audio signal using two triangle windows offset by their width to
    #so that when they combine together, we get as flat of a blending as possible

    Output = Reconstruct2
    Output[Offset] += Reconstruct2Odd
    Output = np.array(Output,dtype=np.float32)
    return np.array((Output),dtype=np.float32)
