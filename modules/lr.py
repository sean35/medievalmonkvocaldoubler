import librosa

def pitchShift(data, samplerate, pitch):
    return librosa.effects.pitch_shift(data, sr=samplerate, n_steps=pitch, bins_per_octave=12) 