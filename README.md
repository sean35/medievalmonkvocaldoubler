# MedievalMonkVocalDoubler
  
# Team Members
Matthew Cole (mattcole@pdx.edu)   
Sean Gilbert (sean35@pdx.edu)
  
# Description

The goal of this project is to use pitchshifting of an original audio signal to help recreate the sound of medieval parallel organum. This is done by playing the shifted audio along with the original audio. To recreate the organum sound, all that's really needed is fourths and octaves, but the software can theoretically handle any interval(s). This can also handle multiple pitchshifts in parallel to help bulk up the sound and emulate more types of parallel movement. The main project can be configured from the command line to use different pitch shifting modules. This serves a handful of different purposes: it allows us to use provide both our own handbuilt pitch shifting system as well as a standard pitch shifting system (so we don't have to throw away the effort we put into it); it allows faster/simpler iteration by allowing us to side by side compare how different pitch systems effect the final output (compared to say, using multiple git branches or multiple full programs); and finally it provides nice encapsulation.
  
# Modules
  
## LR pitch module (default)
  
3 inputs; (.wav) data, Samplerate, pitch
This module works by calling the librosa python library to pitch shift the audio.
  
## RS pitch module (Call using "--pmodule rs")
  
3 inputs; (.wav) data, Samplerate, pitch
    pitch input; use a just-intonation interval ratio table to move pitch up or down
    
This module works by cutting up an audio sample in a set of grains, then resampling each grain to the required pitch shift requirement, manipulating each grain to maintain the correct length to play alongside the original audio sample. 
- To reduce discontinuities, a pair of bartlett/triangle windows are applied to each grain. This is done where one set of windows placed end-to-end and another set are placed end-to-end, but offset by 50% such that the sum for both pairs of is equal to one. 
- The method of manipulating grains to maintain the same length as the audio sample is simple. For resampling that extends the signal, the code simply trims off the excess. For resampling that shortens the signal, while the resampled output is shorter than the grain size requirement, we append a reversed version. Then if it is still too short, then just append copies. Then trim down excess.
  
# Usage
  
usage: MonkDoubler.py [-h] [--volume VOLUME] [--pitch PITCH] [--pmodule PMODULE] [--graph] [--out OUT] [--wav WAV]  
  
options:  
&emsp;  -h, --help         show this help message and exit  
&emsp;  --volume VOLUME    volume in 3dB units (default 9 = 0dB, 0 = 0 output)  
&emsp;  --pitch PITCH      pitch modifiers in quotation marks separated by spaces in half steps. e.g. "5, 12, 17"  
&emsp;  --pmodule PMODULE  pitch module to use  
&emsp;  --graph            provides a graph of the output audio file  
&emsp;  --out OUT          write to WAV file instead of playing  
&emsp;  --wav WAV          input audio file  
  
You can use the pmodule option to use different pitch shifting implementations. You can switch between our custom pitch module using "--pmodule rs" or use the default of librosa.  

# Result

This project is able to create parallel repitched voices alongside an original audio signal. There are some caveats and limitations, but it does what was intended.
## limitations
- Limitation 1: Can't pitch up or down too high, especially going up. Any distortion from repitching is far more audible. While the distortion isn't super strong, it is audible when there are lots of voices. Especially if the audio clip has noise in it. 
- Limitation 2: Not real time. Limited to using preexisting audio and would likely have lag and  audible phasing if it was real-time. 

## Improvements/Future
- Improvement 1: Try to implement as close to real time as possible. Would need a significant reworking
- Improvement 2: Clean up audio before processing to limit distortion. This can be in the form of noise gates or some specific noise reduction alghorithm. (Wavelets?)